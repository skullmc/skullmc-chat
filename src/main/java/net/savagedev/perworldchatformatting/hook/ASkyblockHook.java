package net.savagedev.perworldchatformatting.hook;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import org.bukkit.entity.Player;

public class ASkyblockHook implements SkyblockHook {
    @Override
    public long getIslandLevel(Player player) {
        return ASkyBlockAPI.getInstance().getLongIslandLevel(player.getUniqueId());
    }
}
