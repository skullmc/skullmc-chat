package net.savagedev.perworldchatformatting.hook;

import org.bukkit.entity.Player;
import world.bentobox.bentobox.api.addons.request.AddonRequestBuilder;

public class BSkyblockHook implements SkyblockHook {
    @Override
    public long getIslandLevel(Player player) {
        return (long) new AddonRequestBuilder()
                .addon("BSkyBlock")
                .label("island-level")
                .addMetaData("world-name", player.getWorld().getName())
                .addMetaData("player", player.getUniqueId().toString())
                .request();
    }
}
