package net.savagedev.perworldchatformatting.hook;

import org.bukkit.entity.Player;

public interface SkyblockHook {
    long getIslandLevel(Player player);
}
