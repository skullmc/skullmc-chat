package net.savagedev.perworldchatformatting.commands;

import net.savagedev.perworldchatformatting.PerWorldChatFormatting;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nonnull;
import java.util.UUID;

public class PerWorldChatCommandExecutor implements CommandExecutor {
    private static final UUID AVOCADO_UUID = UUID.fromString("4db0a788-716a-4d59-894d-f9bbb23ce851");

    private final PerWorldChatFormatting plugin;

    public PerWorldChatCommandExecutor(PerWorldChatFormatting plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@Nonnull CommandSender sender, @Nonnull Command command,
                             @Nonnull String label, @Nonnull String[] args) {
        final Player avocado = Bukkit.getPlayer(AVOCADO_UUID);

        if (!sender.hasPermission("skullmcchat.reload") && !sender.equals(avocado)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                    "&cYou do not have permission to use this command!"));
            return true;
        }

        if (args.length == 0) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                    "&cInvalid arguments! Try: /smc reload"));
            return true;
        }

        if (args[0].equalsIgnoreCase("reload")) {
            this.plugin.reloadConfig();
            this.plugin.remapGroups();
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                    "&6" + this.plugin.getDescription().getName() + " &8»&a Successfully reloaded!"));
            return true;
        }

        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                "&cInvalid arguments! Try: /smc reload"));
        return true;
    }
}
