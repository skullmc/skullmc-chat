package net.savagedev.perworldchatformatting.listeners;

import net.savagedev.perworldchatformatting.PerWorldChatFormatting;
import net.savagedev.perworldchatformatting.model.ChatFormattingGroup;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;


public class ChatListener implements Listener {
    private final PerWorldChatFormatting plugin;

    public ChatListener(PerWorldChatFormatting plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onChatE(AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final ChatFormattingGroup formattingGroup = this.plugin.getGroup(player.getWorld());

        if (formattingGroup == null) {
            return;
        }

        event.getRecipients().clear();
        event.setFormat(ChatColor.translateAlternateColorCodes('&', formattingGroup.getChatFormat())
                .replace("{player}", player.getDisplayName())
                .replace("{prefix}", this.plugin.getChat().getPlayerPrefix(player))
                .replace("{suffix}", this.plugin.getChat().getPlayerSuffix(player))
                .replace("{world}", player.getWorld().getName())
                .replace("{island_level}", String.valueOf(this.plugin.getSkyblockHook().getIslandLevel(event.getPlayer())))
                .replace("{message}", "%2$s")
        );

        for (World world : formattingGroup.getWorlds()) {
            event.getRecipients().addAll(world.getPlayers());
        }
    }
}
