package net.savagedev.perworldchatformatting;

import net.milkbowl.vault.chat.Chat;
import net.savagedev.perworldchatformatting.commands.PerWorldChatCommandExecutor;
import net.savagedev.perworldchatformatting.hook.ASkyblockHook;
import net.savagedev.perworldchatformatting.hook.BSkyblockHook;
import net.savagedev.perworldchatformatting.hook.DefaultSkyblockHook;
import net.savagedev.perworldchatformatting.hook.SkyblockHook;
import net.savagedev.perworldchatformatting.listeners.ChatListener;
import net.savagedev.perworldchatformatting.model.ChatFormattingGroup;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PerWorldChatFormatting extends JavaPlugin {
    private final Map<World, ChatFormattingGroup> groups = new HashMap<>();

    private SkyblockHook skyblockHook;

    private Chat chat;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.remapGroups();
        this.hookVault();
        this.hookSkyblock();
        this.initCommand();
        this.initListeners();
    }

    private void hookVault() {
        final RegisteredServiceProvider<Chat> chatProvider = this.getServer()
                .getServicesManager()
                .getRegistration(Chat.class);
        if (chatProvider == null) {
            this.getLogger().severe("This plugin requires Vault to be installed! Disabling plugin...");
            this.getServer().getPluginManager().disablePlugin(this);
        } else {
            this.chat = chatProvider.getProvider();
        }
    }

    private void hookSkyblock() {
        if (this.getServer().getPluginManager().isPluginEnabled("ASkyBlock")) {
            this.skyblockHook = new ASkyblockHook();
        } else if (this.getServer().getPluginManager().isPluginEnabled("BSkyBlock")) {
            this.skyblockHook = new BSkyblockHook();
        } else {
            this.skyblockHook = new DefaultSkyblockHook();
        }
    }

    public void remapGroups() {
        final ConfigurationSection groupSection = this.getConfig().getConfigurationSection("groups");

        if (groupSection == null) {
            this.getLogger().warning("No chat groups configured!");
            return;
        }

        for (String groupName : groupSection.getKeys(false)) {
            final List<String> worldNames = groupSection.getStringList(groupName);

            if (worldNames.isEmpty()) {
                this.getLogger().warning("No worlds found for group '" + groupName + "!'");
                continue;
            }

            final String chatFormat = this.getConfig().getString("format." + groupName);

            if (chatFormat == null) {
                this.getLogger().warning("No format configured for group '" + groupName + "!'");
                continue;
            }

            final ChatFormattingGroup group = new ChatFormattingGroup(groupName, chatFormat);

            for (String worldName : worldNames) {
                final World world = Bukkit.getWorld(worldName);

                if (world == null) {
                    this.getLogger().warning("World '" + worldName + "' in group '" + groupName + "' not loaded!");
                    continue;
                }

                group.addWorld(world);
                this.groups.put(world, group);
            }

            this.getLogger().info("Successfully loaded group '" + group.getName() + "' with " + group.getWorlds().size() + " worlds.");
        }
    }

    public void initCommand() {
        this.getCommand("skullmcchat").setExecutor(new PerWorldChatCommandExecutor(this));
    }

    public void initListeners() {
        this.getServer().getPluginManager().registerEvents(new ChatListener(this), this);
    }

    public ChatFormattingGroup getGroup(World world) {
        return this.groups.get(world);
    }

    public SkyblockHook getSkyblockHook() {
        return this.skyblockHook;
    }

    public Chat getChat() {
        return this.chat;
    }
}
