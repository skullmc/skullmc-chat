package net.savagedev.perworldchatformatting.model;

import org.bukkit.World;

import java.util.HashSet;
import java.util.Set;

public class ChatFormattingGroup {
    private final Set<World> worlds = new HashSet<>();

    private final String chatFormat;
    private final String name;

    public ChatFormattingGroup(String name, String chatFormat) {
        this.name = name;
        this.chatFormat = chatFormat;
    }

    public void addWorld(World world) {
        this.worlds.add(world);
    }

    public Set<World> getWorlds() {
        return this.worlds;
    }

    public String getChatFormat() {
        return this.chatFormat;
    }

    public String getName() {
        return this.name;
    }
}
