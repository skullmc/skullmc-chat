plugins {
    id("java")
}

group = "net.savagedev"
version = "1.2.1-SNAPSHOT"

repositories {
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots")
    maven("https://repo.codemc.org/repository/maven-public")
    maven("https://jitpack.io")

    mavenCentral()
}

dependencies {
    compileOnly("org.spigotmc:spigot-api:1.20.6-R0.1-SNAPSHOT")

    compileOnly("com.wasteofplastic:askyblock:3.0.9.4")
    compileOnly("world.bentobox:bentobox:2.4.0-SNAPSHOT")
    compileOnly("com.github.MilkBowl:VaultAPI:1.7")
}

tasks {
    processResources {
        duplicatesStrategy = DuplicatesStrategy.INCLUDE

        from(sourceSets.main.get().resources.srcDirs) {
            expand(Pair("version", project.version))
                .include("plugin.yml")
        }
    }
}
