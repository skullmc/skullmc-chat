# README #

It's a readme doc. Idk what to put here, but something looks better than nothing here.

### SkullMC-Chat ###

* Chat plugin for SkullMC.
* 1.2

### How do I get set up? ###

* Put it in your plugins folder.
* Start your server.
* Configure the groups in the config.
* Type "/smc reload" - To reload the config.
* Should be all good.

### Contribution guidelines. ###

* Writing tests.
* Code review.
* Other guidelines.

### Who do I talk to? ###

* Repo owner.